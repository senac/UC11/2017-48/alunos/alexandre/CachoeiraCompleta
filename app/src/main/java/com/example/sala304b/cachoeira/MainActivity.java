package com.example.sala304b.cachoeira;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.sala304b.cachoeira.Cachoeira;

import java.util.ArrayList;
import java.util.List;




public class MainActivity extends AppCompatActivity {

    private ListView ListView ;
    public static final int REQUEST_NOVO = 1;
    public static final String CACHOEIRA = "cachoeira";
    private Cachoeira cachoeiraSelecionada;

    private List<Cachoeira> lista = new ArrayList<>();
    private BancodeDadosCachoeira BancodeDadosCachoeira;
    private AdapterCachoeira adapterCachoeira;
    private ArrayAdapter<Cachoeira> adapter;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    public void novo(MenuItem item) {
        Intent intent = new Intent(this, NovoActivity.class);
        startActivity(intent);
    }

    public void ajuda(MenuItem item) {
        Intent intent = new Intent(this, SobreActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ListView = findViewById(R.id.ListViewCachoeira);
        BancodeDadosCachoeira = new BancodeDadosCachoeira(this);
        lista = BancodeDadosCachoeira.getLista();
        BancodeDadosCachoeira.close();

        adapter = new ArrayAdapter<Cachoeira>(this, android.R.layout.simple_list_item_1, lista);

        ListView.setAdapter(adapter);


        ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View contexto, int posicao, long indice) {
                cachoeiraSelecionada = (Cachoeira) adapter.getItemAtPosition(posicao);
                Intent intent = new Intent(MainActivity.this, DetalheActivity.class);
                intent.putExtra(CACHOEIRA, cachoeiraSelecionada);
                startActivity(intent);
            }
        });

        registerForContextMenu(ListView);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        cachoeiraSelecionada = (Cachoeira) adapter.getItem(info.position);

        MenuItem itemMenuSite = menu.add("Visitar site");
        Intent intentSite = new Intent(Intent.ACTION_DEFAULT);
        String site = "www.google.com";
        if (!site.startsWith("http://")) {
            site = "http://" + site;
        }
        intentSite.setData(Uri.parse(site));
        itemMenuSite.setIntent(intentSite);

        menu.add("Enviar E-mail");
        menu.add("Ligar");
        menu.add("Como Chegar");
    }

    public void novo1(MenuItem item) {
        Intent intent = new Intent(this, NovoActivity.class);
        startActivityForResult(intent, REQUEST_NOVO);

    }
    @Override
    protected void onActivityResult(int resquestCode,int resultCode, Intent data){

        if (resquestCode == REQUEST_NOVO){
            switch (resquestCode){
                case RESULT_OK:
                    Cachoeira cachoeira = (Cachoeira) data.getSerializableExtra(MainActivity.CACHOEIRA);
                    lista.add(cachoeira);
                    adapter.notifyDataSetChanged();
                    break;
                case RESULT_CANCELED:
                    Toast.makeText(this,"Cancelou",Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }



}
