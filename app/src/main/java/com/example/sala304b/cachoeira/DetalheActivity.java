package com.example.sala304b.cachoeira;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class DetalheActivity extends AppCompatActivity {

    private Cachoeira cachoeira;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe);

        Intent intent= getIntent();

        cachoeira = (Cachoeira) intent.getSerializableExtra(MainActivity.CACHOEIRA);

        if (cachoeira != null){
            ImageView imageView = findViewById(R.id.imagem);
            TextView textViewNome = findViewById(R.id.titulo);
            TextView textViewInformacoes = findViewById(R.id.informacoes);
            RatingBar ratingBarClassificacao = findViewById(R.id.rtClassificacao);

            if (cachoeira.getImagem()!=null){
                Bitmap fotoOriginal = BitmapFactory.decodeFile(cachoeira.getImagem());

                Bitmap fotoReduzida = Bitmap.createScaledBitmap(fotoOriginal, 200, 200, true);

                imageView.setImageBitmap(fotoReduzida);
            }
            textViewNome.setText(cachoeira.getNome());
            textViewInformacoes.setText(cachoeira.getInformocoes());
            ratingBarClassificacao.setRating(cachoeira.getClassificacao());
        }
    }
}
