package com.example.sala304b.cachoeira;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.example.sala304b.cachoeira.Cachoeira;

public class NovoActivity extends AppCompatActivity {


    private ImageView imageView;
    private EditText editTextNome;
    private EditText editTextInformacoes;
    private EditText editTextEmail;
    private EditText editTextTelefone;
    private EditText editTextEndereco;
    private EditText editTextSite;
    private RatingBar ratingBarClassificacao;
    private Button buttonSalvar;
    private BancodeDadosCachoeira BancodeDadosCachoeira;
    private String novoArquivio;
    private static final int CAPTURA_IMAGEM = 1  ;

    private Cachoeira cachoeira;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo);
        imageView = findViewById(R.id.imagem);
        editTextNome = findViewById(R.id.nome);
        editTextInformacoes = findViewById(R.id.informacoes);
        editTextEmail = findViewById(R.id.email);
        editTextTelefone = findViewById(R.id.telefone);
        editTextEndereco = findViewById(R.id.endereco);
        editTextSite = findViewById(R.id.site);
        ratingBarClassificacao = findViewById(R.id.rtClassificacao);
        buttonSalvar = findViewById(R.id.btnSalvar);
        Intent intent = getIntent();
        cachoeira= (Cachoeira)intent.getSerializableExtra(MainActivity.CACHOEIRA);
        if (cachoeira==null){
            cachoeira= new Cachoeira();
        }else {
            preencherActivity(cachoeira);
        }

        buttonSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cachoeira = new Cachoeira();
                cachoeira.setNome(editTextNome.getText().toString());
                cachoeira.setInformocoes(editTextInformacoes.getText().toString());
                cachoeira.setEmail(editTextEmail.getText().toString());
                cachoeira.setTelefone(editTextTelefone.getText().toString());
                cachoeira.setEndereco(editTextEndereco.getText().toString());
                cachoeira.setSite(editTextSite.getText().toString());
                cachoeira.setClassificacao(ratingBarClassificacao.getRating());

                try {
                    BancodeDadosCachoeira = new BancodeDadosCachoeira(NovoActivity.this);
                    BancodeDadosCachoeira.salvar(cachoeira);
                    BancodeDadosCachoeira.close();
                    Toast.makeText(NovoActivity.this, "Salvado com Sucesso.", Toast.LENGTH_LONG).show();
                    finish();

                } catch (Exception ex) {
                    Toast.makeText(NovoActivity.this, "Erro ao tentar Salvar.", Toast.LENGTH_LONG).show();

                }
            }

        });
    }


    private boolean IsPreenchido(String valeu) {
        return (valeu != null && !valeu.isEmpty());

    }

    public void valiarDados() throws Exception {
        List<String> listaCamposRequeridos = new ArrayList<>();
        if (!IsPreenchido(cachoeira.getNome())) {
            listaCamposRequeridos.add("Nome");
            editTextNome.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        }
        if (!IsPreenchido(cachoeira.getInformocoes())) {
            listaCamposRequeridos.add("Informaçõe");
        }
        if (!IsPreenchido(cachoeira.getEmail())) {
            listaCamposRequeridos.add("Email");
        }
        if (!IsPreenchido(cachoeira.getTelefone())) {
            listaCamposRequeridos.add("Telefone");
        }
        if (!IsPreenchido(cachoeira.getEndereco())) {
            listaCamposRequeridos.add("Endereço");
        }
        if (!IsPreenchido(cachoeira.getSite())) {
            listaCamposRequeridos.add("Site");
        }
        if (listaCamposRequeridos.size() > 0) {
            throw new Exception("Campo requisitado(s)" + listaCamposRequeridos.toString());
        }

    }

    public void salvar(View view) {
        try {
            String nome = editTextNome.getText().toString();
            String informacao = editTextInformacoes.getText().toString();
            String email = editTextEmail.getText().toString();
            String telefone = editTextTelefone.getText().toString();
            String endereco = editTextEndereco.getText().toString();
            String site = editTextSite.getText().toString();

            float classificacao = ratingBarClassificacao.getRating();

            cachoeira = new Cachoeira();
            cachoeira.setNome(nome);
            cachoeira.setInformocoes(informacao);
            cachoeira.setClassificacao(classificacao);

            cachoeira.setEmail(email);
            cachoeira.setTelefone(telefone);
            cachoeira.setEndereco(endereco);
            cachoeira.setSite(site);

            this.valiarDados();


            BancodeDadosCachoeira = new BancodeDadosCachoeira(this) ;
            BancodeDadosCachoeira.salvar(cachoeira);
            BancodeDadosCachoeira.close();

            finish();
        } catch (Exception ex) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("Erro de Validação").setMessage(ex.getMessage());
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }
    private void preencherActivity(Cachoeira cachoeira) {

        editTextNome.setText(cachoeira.getNome());
        editTextInformacoes.setText(cachoeira.getInformocoes());
        ratingBarClassificacao.setRating((float) cachoeira.getClassificacao());

        if(cachoeira.getImagem() != null) {
            Bitmap fotoOriginal = BitmapFactory.decodeFile(cachoeira.getImagem());

            Bitmap fotoReduzida = Bitmap.createScaledBitmap(fotoOriginal, 200, 200, true);

            imageView.setImageBitmap(fotoReduzida);
        }

    }
    public void pegarImagem(View view){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String caminhoDoArquivo = Environment.getExternalStorageDirectory().toString() ;


        novoArquivio = caminhoDoArquivo + "/" + System.currentTimeMillis() + ".png";

        File arquivoImagem = new File(novoArquivio) ;

        Uri localDeArmazenamento = Uri.fromFile( arquivoImagem ) ;

        intent.putExtra(MediaStore.EXTRA_OUTPUT , localDeArmazenamento ) ;



        startActivityForResult(intent , CAPTURA_IMAGEM);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(CAPTURA_IMAGEM == requestCode){
            if(resultCode == RESULT_OK){


                Bitmap fotoOri = BitmapFactory.decodeFile(novoArquivio) ;

                Bitmap fotoFormt = Bitmap.createScaledBitmap(fotoOri , 200 , 200 , true);

                imageView.setImageBitmap(fotoFormt);

            }
        }

    }
}
